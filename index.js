const express = require("express")
const app = express()
var bodyParser = require('body-parser')
var ObjectId = require('mongodb').ObjectID;
var methods = require('methods')

/**
 * Import MongoClient & connexion à la DB
 */
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'todo';
let db

MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");
    db = client.db(dbName);
  });

app.use(express.json())
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
    extended: true
  }));

app.get('/', async(req, res) => {
    let resBacklogTmp = await db.collection('todo_collection').find({status : "A faire"}) 
    let resDoingTmp = await db.collection('todo_collection').find({status : "En cours"})
    let resDoneTmp = await db.collection('todo_collection').find({status : "Terminée"})
    
    let resBacklog = await resBacklogTmp.toArray()
    let resDoing = await resDoingTmp.toArray()
    let resDone = await resDoneTmp.toArray()

    res.render(__dirname + "/main.ejs", {dataBacklog: resBacklog, dataDoing : resDoing, dataDone : resDone});
});

app.post('/updateBacklogTask', async (req, res) => {
    try{
        console.log(await req.body.description)
        await db.collection('todo_collection').updateOne(
         
            { _id:  ObjectId(await req.body.id)}, {$set :      
            {
                "status" : "A faire",
                "description": await req.body.description,
                "membre": "NM",
                "title": await req.body.title,
                "date": Date.now()
            },
        },
        { upsert: true }
        , 
        function(err, res) {
            if (err) throw err;
            else {
                console.log("updated")
            }
        });
        res.redirect(301, "/");
    }
    catch(e){
        throw e
    }   
});

app.post('/updateDoingTask', async (req, res) => {
    try{
        await db.collection('todo_collection').updateOne(
            { _id:  ObjectId(await req.body.id)}, {$set :      
            {
                "status" : "En cours",
                "description": await req.body.description,
                "membre": "NM",
                "title": await req.body.title,
                "date": Date.now()
            },
        },
        { upsert: true }
        , 
        function(err, res) {
            if (err) throw err;
            else {
                console.log("updated")
            }
        });
        res.redirect(301, "/");
    }
    catch(e){
        throw e
    }   
});

app.post('/updateDoneTask', async (req, res) => {
    try{
        console.log(await req.body.description)
        await db.collection('todo_collection').updateOne(
         
            { _id:  ObjectId(await req.body.id)}, {$set :      
            {
                "status" : "Terminée",
                "description": await req.body.description,
                "membre": "NM",
                "title": await req.body.title,
                "date": Date.now()
            },
        },
        { upsert: true }
        , 
        function(err, res) {
            if (err) throw err;
            else {
                console.log("updated")
            }
        });
        res.redirect(301, "/");
    }
    catch(e){
        throw e
    }   
});

app.post('/moveToBacklog', async (req, res) => {
    try{
        await db.collection('todo_collection').updateOne(
         
            { _id:  ObjectId(await req.body.id)}, {$set :      
            {
                "status" : "A faire"
            },
        },
        { upsert: true }
        , 
        function(err, res) {
            if (err) throw err;
            else {
                console.log("updated")
            }
        });
        res.redirect(301, "/");
    }
    catch(e){
        throw e
    }   
});

app.post('/moveToDoing', async (req, res) => {
    try{
        await db.collection('todo_collection').updateOne(
         
            { _id:  ObjectId(await req.body.id)}, {$set :      
            {
                "status" : "En cours"
            },
        },
        { upsert: true }
        , 
        function(err, res) {
            if (err) throw err;
            else {
                console.log("updated")
            }
        });
        res.redirect(301, "/");
    }
    catch(e){
        throw e
    }   
});


app.post('/moveToDone', async (req, res) => {
    try{
        await db.collection('todo_collection').updateOne(
         
            { _id:  ObjectId(await req.body.id)}, {$set :      
            {
                "status" : "Terminée"
            },
        },
        { upsert: true }
        , 
        function(err, res) {
            if (err) throw err;
            else {
                console.log("updated")
            }
        });
        res.redirect(301, "/");
    }
    catch(e){
        throw e
    }   
});

app.post('/addBacklogTask', async(req, res) => {
    await db.collection('todo_collection').insertOne(
        {
        "status" : "A faire",
        "description": await req.body.description,
        "membre": "NM",
        "title": await req.body.title,
        "date": Date.now()
        }, 
        function(err, res) {
            if (err) throw err;
            console.log("1 document inserted");
        }
    );   

    res.redirect(301, "/");
});

app.post('/addDoingTask', async(req, res) => {
    if(req.body.description == null || req.body.description == ""){
        res.status(500).send("Unlucky")
    }
    else{
        await db.collection('todo_collection').insertOne(
            {
            "status" : "En cours",
            "description": await req.body.description,
            "membre": "NM",
            "title": await req.body.title,
            "date": Date.now()
            }, 
            function(err, res) {
                if (err) throw err;
                console.log("1 document inserted");
            }
        );   
    
        res.redirect(301, "/");
    }
});

app.post('/addDoneTask', async(req, res) => {
    await db.collection('todo_collection').insertOne(
        {
        "status" : "Terminée",
        "description": await req.body.description,
        "membre": "NM",
        "title": await req.body.title,
        "date": Date.now()
        }, 
        function(err, res) {
            if (err) throw err;
            console.log("1 document inserted");
        }
    );   

    res.redirect(301, "/");
});

app.post("/deleteTask", async(req,res) => {
    await db.collection('todo_collection').deleteOne({ _id:  ObjectId(await req.body.id)}, function (err, results) {});
    res.redirect(301, "/");
})

app.listen(8080, () => {
    console.log("Serveur à l'écoute")
})
